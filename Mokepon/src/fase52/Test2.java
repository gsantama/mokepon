package fase52;

import java.util.ArrayList;

public class Test2 {

	public static void main(String[] args) {

		ArrayList<Mokepon> arrayMokepon = new ArrayList<Mokepon>();
		int j=0;
		Tipus tActual = Tipus.AIGUA;
		for (int i = 0; i < 4; i++) {
			if (i%3 == 0) tActual = Tipus.FOC;
			if (i%3 == 1) tActual = Tipus.AIGUA;
			if (i%3 == 2) tActual = Tipus.PLANTA;
			arrayMokepon.add(new Mokepon("Mok" + i, (int) (Math.random() * 15), 10, 10, 10, 10));
			arrayMokepon.get(j++).setTipus(tActual);
			arrayMokepon.add(new Mokepon("Mok" + i, (int) (Math.random() * 15), 10, 10, 10, 10));
			arrayMokepon.get(j++).setTipus(tActual);		
		}
		System.out.println("\nLlista mokepons abans d'ordenar: ");
		for (Mokepon mk: arrayMokepon) 
			System.out.println(mk.pretty());
	
		arrayMokepon.sort(null);

		System.out.println("\nLlista mokepons despr�s d'ordenar: ");
		for (Mokepon mk: arrayMokepon) 
			System.out.println(mk.pretty());
	}
}
