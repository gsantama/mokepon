package fase52;

import java.util.ArrayList;
import java.util.Scanner;

public class Test3 {

	static Scanner sc = new Scanner(System.in);
	
	private static int MokeponMesRapid(Mokepon m1, Mokepon m2) {
		if (m1.getHp_actual() > m2.getHp_actual()) return 1;		
		if (m1.getHp_actual() < m2.getHp_actual()) return 2;
		if (Math.random()*2 == 1)  //arbibrari i per simplificar
			return 1;	
		else
			return 2;
	}
	
	static int triaAtac(Mokepon m) throws Exception {
		int nAtacs = m.getAtac().size();
		int qAtac=0;
		if (nAtacs == 0) throw new Exception ("Mokepon sense atacs");
		if (nAtacs == 1) return 0;
		if (nAtacs > 1) {
			System.out.println("Tria n� atac (0 .. " + (nAtacs-1) + ")");
			qAtac = sc.nextInt();
			if(qAtac > (nAtacs-1) || qAtac <0) throw new Exception ("Atac no existeix");
		}
		return qAtac;
	}

	public static void main(String[] args) {

	Mokepon mokepon1 = new Mokepon();
	Mokepon mokepon2 = new Mokepon();
	Mokepon ataca;
	boolean fi = false;
	
	int torn = MokeponMesRapid(mokepon1,mokepon2);
    while(!fi) {
            //usuari tria el numero d'atac amb un scanner
    	try {
    		if (torn ==1) 
    			ataca = mokepon1;
    		else 
    			ataca = mokepon2;
    		
            int numAtac = triaAtac(ataca);
            //si el torn es 1 mokepon1 ataca a mokepon2, si el torn es 2 al reves. Consulta si el mokepon atacat ha resultat debilitat, i retorna l'estat del Mokepon atacant
            boolean debilitat = gestionarAtac(mokepon1, mokepon2, torn, numAtac);
            //si l'altre Mokepon queda debilitat, s'acaba el combat i s'anuncia el guanyador
            fi = fiCombat(debilitat, torn);
            //es passa el torn al seg�ent
    	}
    	catch (Exception e){
    		System.out.println(e.getMessage());
    	}
    	finally {
            torn = canviTorn(torn);
    	}
    }
    }
    
    
    private static boolean fiCombat(boolean debilitat, int torn) {
		// TODO Auto-generated method stub
		return false;
	}

	private static boolean gestionarAtac(Mokepon mokepon1, Mokepon mokepon2, int torn, int numAtac) {
		// TODO Auto-generated method stub
		return false;
	}

	static int canviTorn(int t) {
    	if (t == 1) return 2;
    	return 1;
    }
	}
